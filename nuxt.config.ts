import { Configuration } from '@nuxt/types';

const { CI_PAGES_URL } = process.env;
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname;

const config: Configuration = {
	mode: 'spa',
	router: {
		base
	},
	generate: {
		dir: 'public'
	},
	buildModules: ['@nuxt/typescript-build', '@nuxtjs/fontawesome'],
	modules: [
		'@nuxtjs/svg',
		[
			'nuxt-buefy',
			{
				materialDesignIcons: false,
				defaultIconPack: 'fas',
				defaultIconComponent: 'FontAwesomeIcon'
			}
		]
	],
	fontawesome: {
		icons: {
			solid: true,
			brands: true
		}
	},
	head: {
		title: 'Iridium'
	}
};

export default config;
