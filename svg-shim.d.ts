declare module '*.svg' {
	const svg: any;

	export default svg;
}

declare module '*.svg?raw' {
	const svg: string;

	export default svg;
}
