# Iridium Download Page

A download page for [Iridium](https://github.com/ParticleCore/Iridium) add-on.

## How does it work?

It uses [Axios](https://github.com/axios/axios) to download `.xpi` file from [jsDelivr CDN](https://www.jsdelivr.com/) that replicate files from GitHub (jsDelivr allows requesting via [CORS](https://developer.mozilla.org/th/docs/Web/HTTP/CORS) from any origin).
Then create [`File`](https://developer.mozilla.org/en-US/docs/Web/API/File) with `application/x-xpinstall` MIME type and download with URL that create via [`URL.createObjectURL()`](https://developer.mozilla.org/en-US/docs/Web/API/URL/createObjectURL).
