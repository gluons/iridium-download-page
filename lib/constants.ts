export const MY_REPO = 'https://gitlab.com/gluons/iridium-download-page';

export const REPO = 'https://github.com/ParticleCore/Iridium';

export const REPO_LATEST_URL =
	'https://github.com/ParticleCore/Iridium/releases/latest';

export const REPO_MASTER_URL =
	'https://github.com/ParticleCore/Iridium/blob/master/dist/Iridium.xpi';

export const MANUAL_INSTALL_LINK =
	'https://github.com/ParticleCore/Iridium/wiki/Download';

export const XPI_MIME = 'application/x-xpinstall';

export const LATEST_URL =
	'https://cdn.jsdelivr.net/gh/ParticleCore/Iridium@latest/dist/Iridium.xpi';

export const MASTER_URL =
	'https://cdn.jsdelivr.net/gh/ParticleCore/Iridium@master/dist/Iridium.xpi';
