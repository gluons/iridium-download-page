import axios, { AxiosError } from 'axios';

import { XPI_MIME } from './constants';

export default async function download(url: string) {
	try {
		const res = await axios.get<Blob>(url, {
			responseType: 'blob'
		});

		const blob = res.data;

		if (!blob) {
			return;
		}

		const file = new File([blob], 'Iridium.xpi', {
			type: XPI_MIME
		});
		const objectUrl = window.URL.createObjectURL(file);

		window.location.assign(objectUrl);
	} catch (err) {
		// Extract error message from response body.
		if (err.response) {
			const axiosErr = err as AxiosError<Blob>;
			const rawData = axiosErr.response?.data;
			const errMsg = await rawData?.text();

			throw new Error(errMsg);
		}

		throw err;
	}
}
